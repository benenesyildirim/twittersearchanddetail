package com.benenesyildirim.twittersearchanddetail.CustomAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.benenesyildirim.twittersearchanddetail.DataModel.UserProperties;
import com.benenesyildirim.twittersearchanddetail.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<UserProperties> users;
    private UserOnClickListener listener;

    public UserListAdapter(List<UserProperties> users, UserOnClickListener listener) {
        this.users = users;
        this.listener = listener;
    }

    public interface UserOnClickListener{
        void UserOnItemClick(UserProperties tv);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.searchlist_row_design,null);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        ItemHolder itemHolder = (ItemHolder) holder;

        String uri = "http://pbs.twimg.com/" + users.get(position).getProfile_background_image_url_https();

        Picasso.get().load(uri).into(itemHolder.profilePhoto);
        itemHolder.userName.setText(users.get(position).getName());
        itemHolder.tweet.setText(users.get(position).getDescription());

        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.UserOnItemClick(users.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder{
        ImageView profilePhoto;
        TextView  userName,tweet;

        ItemHolder(View view){
            super(view);
            profilePhoto = view.findViewById(R.id.user_profile_photo_iv);
            userName = view.findViewById(R.id.username_tv);
            tweet = view.findViewById(R.id.description_tv);
        }
    }

    public void onRestart(){

    }
}
