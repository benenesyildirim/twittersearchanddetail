package com.benenesyildirim.twittersearchanddetail.CustomAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.benenesyildirim.twittersearchanddetail.DataModel.TweetProperties;
import com.benenesyildirim.twittersearchanddetail.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TimelineAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<TweetProperties> tweets;

    public TimelineAdapter(List<TweetProperties> tweets) {
        this.tweets = tweets;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.timeline_row_design, null);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        TimelineAdapter.ItemHolder itemHolder = (TimelineAdapter.ItemHolder) holder;

        Picasso.get().load(tweets.get(position).getImage_source()).into(itemHolder.image);
        itemHolder.tweet.setText(tweets.get(position).getText());
    }

    @Override
    public int getItemCount() {
        return tweets.size();
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView tweet;

        ItemHolder(View view) {
            super(view);
            image = view.findViewById(R.id.tweet_image_iv);
            tweet = view.findViewById(R.id.timeline_tweet_tv);
        }
    }
}