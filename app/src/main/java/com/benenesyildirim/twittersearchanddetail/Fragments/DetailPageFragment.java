package com.benenesyildirim.twittersearchanddetail.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.benenesyildirim.twittersearchanddetail.CustomAdapters.TimelineAdapter;
import com.benenesyildirim.twittersearchanddetail.CustomAdapters.UserListAdapter;
import com.benenesyildirim.twittersearchanddetail.DataModel.TweetList;
import com.benenesyildirim.twittersearchanddetail.DataModel.UserList;
import com.benenesyildirim.twittersearchanddetail.DataModel.UserProperties;
import com.benenesyildirim.twittersearchanddetail.DataService.API;
import com.benenesyildirim.twittersearchanddetail.DataService.RequestInterface;
import com.benenesyildirim.twittersearchanddetail.R;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPageFragment extends Fragment implements UserListAdapter.UserOnClickListener {
    private View view;
    private ImageView banner, pp;
    private TextView userNameTxt, biographyTxt;
    private TimelineAdapter timelineAdapter;
    private RecyclerView timelineRV;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.detail_page_fragment, container, false);

        initView();
        getUserDetails();
        getTimeline();

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void getTimeline() {
        assert getArguments() != null;
        UserProperties user = (UserProperties) getArguments().getSerializable("userName");

        RequestInterface requestInterface = API.getClient().create(RequestInterface.class);
        assert user != null;
        Call<TweetList> call = requestInterface.getTimeline(user.getId());

        call.enqueue(new Callback<TweetList>() {
            @Override
            public void onResponse(Call<TweetList> call, Response<TweetList> response) {
                assert response.body() != null;
                timelineAdapter = new TimelineAdapter(response.body().getTweetList());

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                timelineRV.setLayoutManager(linearLayoutManager);
                timelineRV.setAdapter(timelineAdapter);
            }

            @Override
            public void onFailure(Call<TweetList> call, Throwable t) {

            }
        });
    }

    private void initView() {
        banner = view.findViewById(R.id.user_banner_photo);
        pp = view.findViewById(R.id.user_profile_photo_detailed_iv);
        userNameTxt = view.findViewById(R.id.username_tv);
        biographyTxt = view.findViewById(R.id.user_biography);
        timelineRV = view.findViewById(R.id.timeline_list_rv);
    }

    private void getUserDetails() {
        RequestInterface requestInterface = API.getClient().create(RequestInterface.class);
        assert getArguments() != null;
        UserProperties user = (UserProperties) getArguments().getSerializable("userName");
        assert user != null;
        Call<UserList> callSeries = requestInterface.getUsers(user.getName());

        callSeries.enqueue(new Callback<UserList>() {
            @Override
            public void onResponse(Call<UserList> call, Response<UserList> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Picasso.get().load(response.body().getUserListPropertiesList().get(0).getProfile_background_image_url_https()).into(pp);
                    Picasso.get().load(response.body().getUserListPropertiesList().get(0).getProfile_background_image_url_https()).into(banner);
                    userNameTxt.setText(response.body().getUserListPropertiesList().get(0).getScreen_name());
                    biographyTxt.setText(response.body().getUserListPropertiesList().get(0).getDescription());
                }
            }

            @Override
            public void onFailure(Call<UserList> call, Throwable t) {

            }
        });
    }

    @Override
    public void UserOnItemClick(UserProperties tv) {

    }
}
