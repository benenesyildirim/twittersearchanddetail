package com.benenesyildirim.twittersearchanddetail.Fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.benenesyildirim.twittersearchanddetail.CustomAdapters.UserListAdapter;
import com.benenesyildirim.twittersearchanddetail.DataModel.UserList;
import com.benenesyildirim.twittersearchanddetail.DataModel.UserProperties;
import com.benenesyildirim.twittersearchanddetail.DataService.API;
import com.benenesyildirim.twittersearchanddetail.DataService.RequestInterface;
import com.benenesyildirim.twittersearchanddetail.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchPageFragment extends Fragment implements UserListAdapter.UserOnClickListener {

    private EditText searchBar;
    private RecyclerView searchResultList;
    private UserListAdapter userListAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.search_page_fragment, container, false);
        initView(view);

        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Toast.makeText(getContext(),s.toString(),Toast.LENGTH_SHORT).show();
//                listSearchResults(s.toString());
            }
        });

        return view;
    }

     private void initView(View view) {
        searchBar = view.findViewById(R.id.search_bar_et);
        searchResultList = view.findViewById(R.id.search_results_rv);
    }

    private void listSearchResults(String userName) {
        RequestInterface requestInterface = API.getClient().create(RequestInterface.class);
        Call<UserList> callSeries = requestInterface.getUsers(userName);

        callSeries.enqueue(new Callback<UserList>() {
            @Override
            public void onResponse(Call<UserList> call, Response<UserList> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    userListAdapter = new UserListAdapter(response.body().getUserListPropertiesList(), SearchPageFragment.this);

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                    linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    searchResultList.setLayoutManager(linearLayoutManager);
                    searchResultList.setAdapter(userListAdapter);
                }
            }

            @Override
            public void onFailure(Call<UserList> call, Throwable t) {

            }
        });
    }

    @Override
    public void UserOnItemClick(UserProperties userName) {
        DetailPageFragment detailPageFragment = new DetailPageFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("userName", userName);
        detailPageFragment.setArguments(bundle);

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment, detailPageFragment, "DetailFragment");
        transaction.addToBackStack("DetailFragment");
        transaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshFragment();
    }

    private void refreshFragment(){
        Fragment frg = getActivity().getSupportFragmentManager().findFragmentByTag("SearchPage");
        final FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.detach(frg);
        ft.attach(frg);
        ft.commit();
    }
}