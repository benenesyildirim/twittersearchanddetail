package com.benenesyildirim.twittersearchanddetail.DataService;

import com.benenesyildirim.twittersearchanddetail.DataModel.TweetList;
import com.benenesyildirim.twittersearchanddetail.DataModel.UserList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RequestInterface {

    @GET("/1.1/statuses/user_timeline.json")
    Call<TweetList> getTimeline(@Query("query") String searchQuery);

    @GET("1.1/statuses/user_timeline.json?screen_name={name}&count=10")
    Call<UserList> getUsers(@Path("name") String userName);

}
