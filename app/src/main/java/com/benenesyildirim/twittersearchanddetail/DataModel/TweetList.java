package com.benenesyildirim.twittersearchanddetail.DataModel;

import java.util.List;

public class TweetList {

    private List<TweetProperties> tweetList;

    public List<TweetProperties> getTweetList() {
        return tweetList;
    }

    public void setTweetList(List<TweetProperties> tweetList) {
        this.tweetList = tweetList;
    }
}
