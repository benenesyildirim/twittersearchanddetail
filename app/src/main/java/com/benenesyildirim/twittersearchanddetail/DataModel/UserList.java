package com.benenesyildirim.twittersearchanddetail.DataModel;

import java.io.Serializable;
import java.util.List;

public class UserList {

    private List<UserProperties> userListPropertiesList;

    public List<UserProperties> getUserListPropertiesList() {
        return userListPropertiesList;
    }

    public void setUserListPropertiesList(List<UserProperties> userListPropertiesList) {
        this.userListPropertiesList = userListPropertiesList;
    }
}
