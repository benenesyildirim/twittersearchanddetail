package com.benenesyildirim.twittersearchanddetail.DataModel;

import java.io.Serializable;

public class TweetProperties implements Serializable {

    //Tweetlerin yapısını şu siteden gördüğümle yapmaya çalıştım bazı yerlerde yanlış anlaşılmadan
    //dolayı hatalarım olabilir. Tweet içerisindeki image'lerin nasıl gönderildiğine dair bir bilgi bulamadım ancak sanki
    //varmış gibi davranıp "image_source" diye bir değişken tanımlayacağım.

    private String created_at;
    private String id;
    private String text;

    public String getImage_source() {
        return image_source;
    }

    public void setImage_source(String image_source) {
        this.image_source = image_source;
    }

    private String image_source;
    private UserProperties user;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UserProperties getUser() {
        return user;
    }

    public void setUser(UserProperties user) {
        this.user = user;
    }
}
